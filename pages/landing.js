import React, { useContext } from 'react'
import { IpLocationContext } from '../components/iplocation-context'
import Head from 'next/head'
import Layout from '../layout/layout'

const Landing = () => {
  let localPrice = 25000 // Kenyan Shilling
  let exchangeRate = 0;
  const [ipLocation] = useContext(IpLocationContext)
  const { country, currency } = ipLocation

  switch (currency.code) {
    case 'KES':
      exchangeRate = 1;
      break;
    case 'GBP':
      exchangeRate = 0.0076;
      break;
    default:
      break;
  }
  localPrice = (localPrice * exchangeRate).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')

  return (
    <Layout>
      <Head>
        <title>Landing</title>
      </Head>

      <section className="hero is-warning">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">Landing Page</h1>
            <h2 className="subtitle">Product is available in {country}!</h2>
            <button className="button is-link"><strong>Order Now - </strong> {currency.symbol} {localPrice} </button>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Landing
