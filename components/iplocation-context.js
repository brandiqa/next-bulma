import React, {useState, useEffect, useRef, createContext} from 'react'

export const IpLocationContext = createContext()

export const IpLocationProvider = (props) => {
    const initialState = {
        ipAddress: '0.0.0.0',
        country: 'Nowhere',
        currency: {},
        continent: '',
        isProxy: false,
        proxyType: ''
    }

    const [ipLocation, setIpLocation] = useState(initialState);
    const prev = useRef();

    useEffect(() => {
        if(ipLocation.country == 'Nowhere') {
            const localState = JSON.parse(localStorage.getItem('iplocation'))
            if(localState) {
                console.info('reading local storage')
                prev.current = localState.ipAddress
                setIpLocation(localState)
            }
        } else if(prev.current !== ipLocation.ipAddress) {
            console.info('writing local storage')
            localStorage.setItem('iplocation', JSON.stringify(ipLocation))
        }
    }, [ipLocation])

    return(
        <IpLocationContext.Provider value={[ipLocation, setIpLocation]}>
            {props.children}
        </IpLocationContext.Provider>
    )
}
