import React, { useContext, useEffect, useState }  from 'react'
import Error from 'next/error'
import { useRouter } from 'next/router'
import fetch from 'isomorphic-unfetch'
import Head from 'next/head'
import Layout from '../layout/layout'
import { IpLocationContext } from '../components/iplocation-context'


const Home = ({newProxy, newLocation, errorCode, errorMessage}) => {
  if (errorCode) {
    return <Error statusCode={errorCode} title={errorMessage} />
  }

  const router = useRouter();

  const [proxyType] = useState(newProxy.proxyType)
  // Redirect if Proxy is TOR
  useEffect(() => {
    if(proxyType == 'TOR') {
      router.replace('/abuse')
    }
  }, [proxyType])

  const [ipLocation, setIpLocation] = useContext(IpLocationContext)
  // Store Location and Proxy info
  useEffect(() => {
    let ignore = false
    if(newLocation && !ignore) {
      setIpLocation({...newLocation, ...newProxy})
    }
    return () => { ignore = true; }
  },[newLocation])

  // Redirect based on country
  const { country } = ipLocation
  useEffect(() => {
    if(country != 'Nowhere' && newProxy.proxyType !== 'TOR') {
      redirectPage(router, country)
    }
  }, [ipLocation]);

  return (
    <Layout>
      <Head>
        <title>Home</title>
      </Head>

      <section className="hero is-light">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">Product Page</h1>
            <h2 className="subtitle">Checking availability in your country...</h2>
          </div>
        </div>
      </section>
  </Layout>
  )
}

const redirectPage = (router, country) => {
    let redirectPage;
    switch(country) {
      case 'Kenya':
        redirectPage = '/landing'
        break
      case 'United Kingdom':
          redirectPage = '/landing'
          break
      default:
        redirectPage = '/unavailable'
    }
    router.replace(redirectPage)
}

Home.getInitialProps = async ({ req }) => {
  if(req) {
    let ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress
    const localAddresses = ['::1', '127.0.0.1', 'localhost']
    if(localAddresses.includes(ipAddress))
      ipAddress =  process.env.NEXT_SERVER_TEST_IP || '142.113.220.31' // TOR: '104.149.191.126'

    const proxyResponse = await fetchProxyData(ipAddress)
    if(proxyResponse.errorCode)
      return proxyResponse
    else if(proxyResponse.proxyType === 'TOR')
      return proxyResponse
    else {
      const locationResponse = await fetchLocationData(ipAddress)
      return { ...locationResponse, ...proxyResponse }
    }
  }
  return {newLocation : null}
}

const fetchProxyData = async (ipAddress) => {
  const api_key = process.env.NEXT_SERVER_IP2PROXY_API || 'demo'
  if(!api_key)
    return { errorCode:500, errorMessage: 'IP2PROXY_API key has not been set'}
  const url = `https://api.ip2proxy.com/?ip=${ipAddress}&key=${api_key}&package=PX2`
  try {
    const response = await fetch(url)
    const json = await response.json()
    // console.log(json)
    if(json.response != 'OK')
      return { errorCode:500, errorMessage: json.response }
    const { isProxy,  proxyType } = json
    return { newProxy: {
      isProxy, proxyType
    }}
  } catch (error) {
    console.log(error)
    return { errorCode:error.code, errorMessage: error.message.replace(api_key, 'demo')}
  }
}

const fetchLocationData = async(ipAddress) => {
  const api_key = process.env.NEXT_SERVER_IP2LOCATION_API || 'demo'
  if(!api_key)
    return { errorCode:500, errorMessage: 'IP2Location API Key is Missing in Environment Variables'}
  const url = `https://api.ip2location.com/v2/?ip=${ipAddress}&addon=country,continent&key=${api_key}&package=WS1`
    try {
      const response = await fetch(url)
      const json = await response.json()
      if(json.response)
      return { errorCode:500, errorMessage: json.response}
      const { country, continent, credits_consumed } = json;
      // console.log(json)
      console.log('Location Credits Consumed: ', credits_consumed)
      const newLocation = { ipAddress, country: country.name, continent: continent.name, currency: country.currency }
      return { newLocation }
    } catch (error) {
      return { errorCode:error.code, errorMessage: error.message.replace(api_key, 'demo')}
    }
}

export default Home
