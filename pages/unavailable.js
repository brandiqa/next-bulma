import React, { useContext } from 'react'
import Head from 'next/head'
import Layout from '../layout/layout'
import { IpLocationContext } from '../components/iplocation-context'

const Unavailable = () => {
  const [ipLocation] = useContext(IpLocationContext)
  const { country } = ipLocation

  return (
    <Layout>
      <Head>
        <title>Unavailable</title>
      </Head>

      <section className="hero is-dark">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">Sorry. Product is not available in <strong>{ country }</strong> </h1>
            <h2 className="subtitle">Click to join waiting list</h2>
            <button className="button is-link">Subscribe to Alert Notification</button>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Unavailable
