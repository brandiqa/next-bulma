import React, { useContext } from 'react'
import { IpLocationContext } from './iplocation-context'

const style = {
    padding: 12
}

const IpLocation = () => {
    const [ipLocation] = useContext(IpLocationContext)
    const { ipAddress, country, currency, continent, isProxy, proxyType } = ipLocation

    return (
        <div className="box center" style={style}>
            <div className="content">
                <ul>
                    <li>IP Address : {ipAddress} </li>
                    <li>Currency : {currency.name} - {currency.symbol} </li>
                    <li>Country : {country} </li>
                    <li>Continent : {continent} </li>
                    <li>Proxy : {isProxy} </li>
                    <li>Proxy Type: {proxyType} </li>
                </ul>
            </div>
        </div>
    )
}

export default IpLocation