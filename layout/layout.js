import Head from 'next/head'
import Nav from './nav'
import IpLocation from '../components/iplocation'

const Layout = (props) => (
  <div>
    <Head>
      <title>IP2Location Example</title>
      <link rel='icon' href='/favicon.ico' />
    </Head>
    <Nav />
    <section className="section">
      <div className="container">
        {props.children}
        <IpLocation />
      </div>
    </section>
  </div>
)

export default Layout