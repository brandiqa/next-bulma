import React from 'react'
import App from 'next/app'
import 'bulma/css/bulma.css'
import '../styles/index.css'
import { IpLocationProvider } from '../components/iplocation-context'

export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props

    return (
      <IpLocationProvider>
        <Component {...pageProps} />
      </IpLocationProvider>
    )
  }
}
